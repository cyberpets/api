-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 15, 2023 at 09:51 AM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pet_adaption_web`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`) VALUES
(8, 'jason', '$2y$10$mZDMb3LUHM/dLrBIRBSxsOMn8rKzQun3QCxuw4Yfp29', 'jason@gmail.com'),
(9, 'hatdog', '$2y$10$l32LIZ8he3uqwGTvsOhCdOwMTRrCQ8M8htot2LJxdr1', 'hatdog@gmail.com'),
(10, 'dan', '$2y$10$xfKuA0Npkv9qNjTtphvdKuykydKvVBLO13KDsVnHRpt', 'dan@gmail.com'),
(11, 'dan', '$2y$10$2uM9bgfCRtKxAJJyQkXBNOO3vS8tTfrthLqNZbDq.5E', 'dan@gmail.com'),
(12, 'halaman', '$2y$10$4K1xom5q2G.Ctdec1rsjx.Oq3dM5zfL5Q65EdqSc7sz', 'halaman@gmail.com'),
(13, 'dan', '$2y$10$iWI7wqnwCzSjrIBdtZ68sOZnJYu9yAQhwPCmshPN/pl', 'dan@gmail.com'),
(14, 'dan', '$2y$10$6wTShg.6zKRgIk/fNalvpu/I9.Lhm9nAoXPjdnoVsPR', 'dan@gmail.com'),
(15, 'pogi', '$2y$10$ffIQ2PMzesNHzZ4oV7JhxekzWIbHgS3Y6fDlfozHwjK', 'pogi@gmail.com'),
(16, 'kingina', '$2y$10$3ZX/x/6uttfWgMIBd9tI2erD66rLTkpMUsDTa/UG.rT', 'kingina@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
