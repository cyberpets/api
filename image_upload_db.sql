-- Create the database if it doesn't exist
CREATE DATABASE IF NOT EXISTS image_upload_db;

-- Use the database
USE image_upload_db;

-- Create the 'users' table
CREATE TABLE IF NOT EXISTS users (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    age INT,
    description TEXT,
    image_path VARCHAR(255) NOT NULL
);
