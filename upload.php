<?php
// Database connection
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "flutter_auth";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Form data
$name = $_POST['name'];
$age = $_POST['age'];
$breed = $_POST['breed'];
$description = $_POST['description'];

// File upload
$image_path = "uploads/" . basename($_FILES["image"]["name"]);
if (move_uploaded_file($_FILES["image"]["tmp_name"], $image_path)) {
    // Insert data into the database
    $sql = "INSERT INTO pets (name, age, breed, description, image_path) VALUES ('$name', $age, '$breed', '$description', '$image_path')";

    if ($conn->query($sql) === TRUE) {
        echo "Data uploaded successfully.";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
} else {
    echo "Sorry, there was an error uploading your file.";
}

$conn->close();
?>
