<?php
header('Content-Type: application/json');
// Database connection parameters
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "flutter_auth"; // Name of the database you created

// Create a connection to the MySQL database
$conn = new mysqli($servername, $username, $password, $dbname);

// Check the connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Fetch pet data from the database
$sql = "SELECT * FROM pets";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    $pets = array();
    while ($row = $result->fetch_assoc()) {
        $imageBaseUrl = "http://192.168.103.254/pet_adaption_web/";
        $item = array(
            "id" => $row["id"],
            "name" => $row["name"],
            "age" => $row["age"],
            "breed" => $row["breed"],
            "description" => $row["description"],
            "image_path" => $imageBaseUrl . "/" . $row["image_path"] // Include the base URL
        );
        $pets[] = $item;
    }
    echo json_encode($pets); // Return the array directly
} else {
    echo json_encode(array("message" => "No items found"));
}

// Close the database connection
$conn->close();
?>
